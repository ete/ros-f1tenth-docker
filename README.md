# Docker container containing ros-melodic-desktop-full and f1tenth_simulator

## Build and Run

- Needs docker (and docker-compose if you want)
- Uses X11 to present GUI (could work with Xwayland?) macos and win needs maybe dockerx11? https://wiki.ros.org/docker/Tutorials/GUI

`docker-compose build`  
`docker-compose run --rm talker bash`

Then in bash one can run eg. `roslaunch f1tenth_simulator simulator.launch`

### Extendibility 

- We could run multiple nodes(as docker services)? https://wiki.ros.org/docker/Tutorials/Compose https://docs.ros.org/en/rolling/How-To-Guides/Run-2-nodes-in-single-or-separate-docker-containers.html?highlight=docker
