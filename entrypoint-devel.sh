#!/bin/bash
set -e

# setup ros environment
source "$CATKIN_WORKSPACE/devel/setup.bash"
exec "$@"
