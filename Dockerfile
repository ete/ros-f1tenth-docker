FROM osrf/ros:melodic-desktop-full

ENV CATKIN_WORKSPACE /root/catkin_ws/

RUN apt-get update && apt-get install -y ros-melodic-tf2-geometry-msgs ros-melodic-ackermann-msgs ros-melodic-joy ros-melodic-map-server rosbash git vim
RUN mkdir -p $CATKIN_WORKSPACE/src 
WORKDIR $CATKIN_WORKSPACE/src
RUN git clone https://github.com/f1tenth/f1tenth_simulator.git

WORKDIR $CATKIN_WORKSPACE/
RUN . "/opt/ros/$ROS_DISTRO/setup.sh" && catkin_make
RUN . devel/setup.sh

COPY ./entrypoint-devel.sh /

VOLUME $CATKIN_WORKSPACE

ENTRYPOINT ["/entrypoint-devel.sh"]
CMD ["bash"]
